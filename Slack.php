<?php

namespace Torbor\yii\extensions;

use yii\base\Component;
use GuzzleHttp\Client;

class Slack extends Component
{
    /**
     * @var string, токен приложения или бота
     */
    public $token;

    /**
     * @var \GuzzleHttp\Client
     */
    private $client;

    public function init()
    {
        $this->client = new Client(['base_uri' => 'https://slack.com/api/']);
    }

    /**
     * @param $method string, имя метода из списка https://api.slack.com/methods
     * @param array $options, массив опций метода https://api.slack.com/methods
     * @return \Psr\Http\Message\ResponseInterface http://docs.guzzlephp.org/en/stable/quickstart.html#using-responses
     */
    public function send($method, array $options = [])
    {
        $response = $this->client->post($method, ['form_params' => array_merge($options, ['token' => $this->token])]);
        return $response;
    }
}
