# yii2-slack

[Yii2](http://www.yiiframework.com) extension for [api.slack.com](https://api.slack.com)

## Installation

### Composer

The preferred way to install this extension is through [Composer](http://getcomposer.org/).

Either run

	php composer.phar require torbor/yii2-slack "dev-master"

or add

	"torbor/yii2-slack": "dev-master"

and add into composer.json

    "repositories": [
        {
            "type": "git",
            "url":  "git@bitbucket.org:torbor_team/slack.git"
        }
    ],
    

to the require section of your composer.json

Add code in your components section of application configuration:

	'slack' => [
		'class' => 'Torbor\yii\extensions\Slack',
		'token' => '<token_app>'
	]

## Usage



    \Yii::$app->slack->send('method', [options]);

[Full methods list](https://api.slack.com/methods)